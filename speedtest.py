import time
from mss import mss
import pyautogui as pg


monitor = {
    "left": 0,
    "top": 0,
    "width": 1,
    "height": 1,
}
num = 1

# Mss
time1 = time.time()

for i in range(num):
    # Возмём кусок экрана
    m = mss()
    # Получаем пиксель с экрана монитора
    img1 = m.grab(monitor)
time2 = time.time()
print(time2 - time1)

# Pyautogui
time1 = time.time()
for i in range(num):
    img2 = pg.screenshot(region=(monitor['left'], monitor['top'], monitor['width'], monitor['height']))
    time2 = time.time()
print(time2 - time1)

# PIL
time1 = time.time()
for i in range(num):
    from PIL import ImageGrab
    img3 = ImageGrab.grab(bbox=(monitor['left'], monitor['top'], monitor['width'], monitor['height']))
time2 = time.time()
print(time2 - time1)

# Result
# 0.015134811401367188
# 0.04700922966003418
# 0.035093069076538086
