import pyautogui as pg
import time
import numpy as np
from mss import mss

# Клик по координатам

# Вход в лавку
pg.click(495, 468)
# Ожидание
time.sleep(1)


def get_color_rgb(x, y):
    # Проба цвета с координат
    m = mss()
    monitor = {
        "left": x,
        "top": y,
        "width": 1,
        "height": 1,
    }
    # Получаем пиксель с экрана монитора
    img = m.grab(monitor)

    # Преобразуем этот пиксель в матрицу
    img_arr = np.array(img)
    item = img_arr[0][0]
    r = item[2]
    g = item[1]
    b = item[0]

    return [r, g, b]

# Цвет щита
print(get_color_rgb(240, 240))

# Цвет шапки продовца.
print(get_color_rgb(525, 320))

# Выход из лавки
pg.click(735, 360)
